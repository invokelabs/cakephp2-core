CakePHP 2 Core Files
====================

This is a copy of the core CakePHP files for version 2.
These files originally exist in the standard CakePHP2 file structure at app/lib/Cake.

They have been put in its own repo because
# Git does not support the cloning of a specific folder in the repo
# These files are core files of the CakePHP framework. All other files in the standard installation are simply stubs/placeholders

Always refer to VERSION.txt to check wat version of CakePHP2 it is. 

When upgrading, always get the latest stable version at
http://cakephp.org/

Then simply drag and replace files from app/lib/Cake into this folder